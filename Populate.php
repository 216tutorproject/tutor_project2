<?php
include_once 'models/Student.php';
include_once 'models/Tutor.php';
include_once 'models/Tutee.php';

$first_names = ["Adam", "Anne", "Brianna", "Brian", "Carl", "Catelyn", "Denise", "Doug", "Eric", "Erin", "Franz", "Fiona", "Greg", "Georgia", "Henry"
		, "Hollie", "Ivan", "Ingrid", "Jean", "John", "Kelly", "Ken", "Leopold", "Lindsay"];

$last_names = ["Appleby", "Arnott", "Bailey", "Carlson", "Dunning", "Ericson", "Ferdinand", "Garland", "Gomez", "Highland", "Wright", "Murphy
		", "Martinez", "Girardi", "Lundquist", "Nash", "Woods", "Rodgers", "Simon", "Garfunkel", "Jagger", "Richards", "Jones", "Watts", 
		"Taylor", "Backes", "Thomas"];

shuffle($last_names);

$student_dao = new StudentDao();
$student_count = $student_dao->countStudents();
if($student_count<1) {
	echo "populating students <br>";
	for($i=0; $i<20; $i++) {
		$fname = $first_names[array_rand($first_names)];
		$lname = $last_names[$i];
		$uni_id = strtolower($fname[0])."_".strtolower($lname)."15";
		$pw_digest = md5("password");
		$student = new Student("", $fname, $lname, $uni_id, $pw_digest);
		$student_dao->saveStudent($student);
	}
	echo "finished populating students <br>";
}


$tutor_dao = new TutorDAO();
$tutor_count = TutorDAO::countTutors();
if($tutor_count < 1) {
	echo "populating tutors <br>";
	$students = $student_dao->getAllStudents();
	shuffle($students);
	for($i = 0; $i < 5; $i++) {
		$newTutor = array_pop($students);
		$tutor_dao->createTutor($newTutor);
	}
	echo "finished populating tutors <br>";
}

$tutee_dao = new TuteeDAO();
$tutee_count = $tutee_dao->countTutees();
if($tutee_count < 1) {
	echo "populating tutees <br>";
	$students = $student_dao->getAllStudents();
	shuffle($students);
	for($i = 0; $i < 15; $i++) {
		$newTutee = array_pop($students);
		$tutee_dao->createTutee($newTutee);
	}
	echo "finished populating tutees <br>";
}

?>













