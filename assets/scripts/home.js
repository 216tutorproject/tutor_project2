function resetCurrentTutorSession(json, building, room) {
    $('#building').val('');
    $('#room_num').val('');
	if(String(json.status)=="Session Added") {
        $('#tutor-current-session-tab').text("Current Session");
		$('#start-session-form').hide();
        $('#current-tutor-session').append(json.html);
        $('#tutor-section').show();
        alert("You successfully declared a tutor session");
	}
	else {
        alert(json.status);
    }
};

function resetTuteeSession(json) {
    alert("You successfully joined a session");
    $('#active-tutor-sessions').hide();
    $('#current-tutee-session').append("<p>You are currently attending this session:</p>" + json.html);
};

function deleteFormButtons(id) {
    $(("#hide-form" + id)).remove();
    $(("#rate-tutor" + id)).remove();
    $(("#rate-tutor" + id + "-btn")).remove();
};

function appendReviewButtons(id) {
    $(("#tutor-buttons-" + id)).append("<button tutor='" + id + "' class='thank-you btn btn-info'> Thank You! </button>");
};

function resetAfterRating(json, id) {
    deleteFormButtons(id);
    appendReviewButtons(id);
};

function resetAfterDelete(id) {
    $(("#view-own-rating" + id)).remove();
    $(("#view-review" + id + "-btn")).remove();
    $(("#hide-review" + id)).remove(); 
};

$(document).ready(function() {

    $("#tutor-section").hide();
    $("#toggle-tutee-mode").hide();

    $("#toggle-tutor-mode").click( function() {
        $("#tutee-section").hide();
        $("#toggle-tutor-mode").hide();
        $("#toggle-tutee-mode").show();
        $("#tutor-section").show();
    });

    $("#toggle-tutee-mode").click( function() {
        $("#tutor-section").hide();
        $("#toggle-tutee-mode").hide();
        $("#toggle-tutor-mode").show();
        $("#tutee-section").show();
    });

    $(".view-tutor").click( function() {
        $(this).addClass("hidden");
        var id = $(this).attr("tutor");
        $(("#tutor" + id + "-info")).removeClass("hidden");
        $(('#rate-tutor'+id)).addClass("hidden");
        $(('#rate-tutor' + id + '-btn')).removeClass("hidden");
        $(('#hide-form') + id).addClass("hidden");
        $(("#hide-tutor" + id)).removeClass('hidden');

        $(("#view-own-rating" + id)).addClass('hidden');
        $(("#view-review" + id + "-btn")).removeClass('hidden');
        $(("#hide-review" + id)).addClass('hidden'); 
    });

    $(".hide-tutor").click( function() {
        $(this).addClass("hidden");
        var id = $(this).attr("tutor");
        $(("#tutor" + id + "-info")).addClass("hidden");
        $(("#view-tutor" + id)).removeClass("hidden");
    });

    $(".rate-tutor").click( function() {
        $(this).addClass("hidden");
        var id = $(this).attr("tutor");
        $(("#tutor" + id + "-info")).addClass("hidden");
        $(("#rate-tutor" + id)).removeClass('hidden');
        $(("#view-tutor" + id)).removeClass('hidden');
        $(("#hide-tutor" + id)).addClass('hidden');

        $(("#hide-form" + id)).removeClass("hidden");
        $(('#view-tutor' + id + "-btn")).removeClass("hidden");
    });

    $(".view-review").click( function() {
        $(this).addClass("hidden");
        var id = $(this).attr("tutor");
        $(("#view-own-rating" + id)).removeClass('hidden');
        $(("#hide-review" + id)).removeClass('hidden'); 

        $(("#view-tutor" + id)).removeClass('hidden');
        $(("#hide-tutor" + id)).addClass('hidden');
        $(("#tutor" + id + "-info")).addClass("hidden");
    });

    $(".hide-review").click( function() {
        $(this).addClass("hidden");
        var id = $(this).attr("tutor");
        $(("#view-review" + id + "-btn")).removeClass("hidden");
        $(("#view-own-rating" + id)).addClass('hidden');
    });

    $(".hide-form").click( function() {
        $(this).addClass("hidden");
        var id = $(this).attr("tutor");
        $(("#rate-tutor" + id + "-btn")).removeClass("hidden");
        $(("#rate-tutor" + id)).addClass("hidden");
    });

    $(".delete-tutor-rating").click( function() {
        $(this).addClass("hidden");
        var id = $(this).attr("tutor");
        $.ajax(
            "/tutor_management/index.php",
            {
                type: "POST",
                processData: false,
                data: "grp=Ratings&cmd=deleteRatingAjax&tutorID=" + id,
                dataType: "json",
                success: function(json) {
                    if(json.status === "success") {
                        alert("Rating removed. You can refresh the page to submit a new rating.");
                        resetAfterDelete(id);
                    }
                    else {
                        alert("Rating failed.");
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert("Error: Delete rating ajax request \n" + jqXHR.responseText);
                }
            }
        );
    });

    $(".submit-rating").click( function() {
        var id = $(this).attr("tutor");
        var rating = $(("#tutor" + id + "-rate")).val();
        var comments = $(("#tutor" + id + "-comments-input")).val();
        $.ajax(
            "/tutor_management/index.php",
            {
                type: "POST",
                processData: false,
                data: "grp=Ratings&cmd=submitRatingAjax&tutorID=" + id + "&rating=" + rating + "&comments=" + comments,
                dataType: "json",
                success: function(json) {
                    if(json.status === "success") {
                        alert("Thank you for your feedback.");
                        resetAfterRating(json, id);
                    }
                    else {
                        alert("Rating failed.");
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert("Error: Submit rating ajax request \n" + jqXHR.responseText);
                }
            }
        );
    }
    );
  
	$("#submit-tutor-session").click( function() {
    	var building = $('#building').val()
    	var room = $('#room_num').val()
    	$.ajax(
    		"/tutor_management/index.php",
        	{
        	type: "POST",
        	processData: false,
        	data: "grp=TutorSessions&cmd=addSessionAjax&building=" + building + "&room_num=" + room,
        	dataType: "json",
        	success: function(json) {
        		resetCurrentTutorSession(json, building, room);
        	},
        	error: function(jqXHR, textStatus, errorThrown) {
            	alert("Error: Add Session Ajax Request \n" + jqXHR.responseText);
        	}
        	}
    	);
  	}
    );

    $(".join-link").click( function() {
        var session =  $(this).attr("id");
        $.ajax(
            "/tutor_management/index.php",
            {
                type: "POST",
                processData: false,
                data: "grp=TutorSessions&cmd=joinSessionAjax&sessionID=" + session,
                dataType: "json",
                success: function(json) {
                    if(json.status == "success") {
                        resetTuteeSession(json);
                    }
                    else {
                        alert(json.reason);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert("Error: Join Session Ajax Request \n" + jqXHR.responseText);
                    alert(errorThrown);
                }
            }
        );
    });





});