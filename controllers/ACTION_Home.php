<?php
include_once($_SERVER['DOCUMENT_ROOT']."/tutor_management/models/Student.php");
include_once( $_SERVER['DOCUMENT_ROOT']."/tutor_management/models/TutorSession.php");
#include_once( $_SERVER['DOCUMENT_ROOT']."/tutor_management/models/Rating.php");


session_start();

include($_SERVER['DOCUMENT_ROOT'].'/tutor_management/sessionsHelper.php');

class Action_Home {

    public function home($request) {
    	if(!isset($_SESSION['student']) || !isset($_COOKIE['session_token'])) { 
			header("location: views/sign_in.php");
		}
		elseif(!correctUser()) { #checks to make sure that user has a token that corresponds to the current user 
			header("location: views/sign_in.php");
		}
		else {
			$student = $_SESSION['student'];
			
			#if the user is a tutor, we will need this infomration
			$_SESSION['tutor'] = $student->getTutorIfTutor();
			if($_SESSION['tutor']) {
				$tutor = $_SESSION['tutor'];
				$_SESSION['currentTutorSession'] = TutorSession::getCurrentSession($tutor->id);
				$_SESSION['pastTutorSessions'] = TutorSession::getPastSessions($tutor->id);
			}
			
			#get a list of all sessions that are happening right now
			$_SESSION['activeSessions'] = TutorSession::getActiveSessions();
			$_SESSION['currentTuteeSession'] = TutorSession::findByID($student->getCurrentSessionID());
			$_SESSION['pastTuteeSessions'] = array_reverse(TutorSession::getPastTuteeSessions($student->id));

			$_SESSION['pastTutorIDs'] = $this->getTutorsTaken($_SESSION['pastTuteeSessions']);

			$_SESSION['pastTutorReviews'] = $student->getPastRatings($_SESSION['pastTutorIDs']);

			#gather tutors:
			$tutors = Student::getAllTutors();
			$tutorAssociation = [];
			for($i=0; $i<sizeof($tutors); $i++) {
				$tutors[$i]->student = Student::findByID($tutors[$i]->student_id);
				$tutors[$i]->gatherRatingsData();
			}
			$_SESSION['tutorList'] = $this->sortTutorsAlphabeticallly($tutors);
			header("Location: views/home.php");
		}
	}

	private function sortTutorsAlphabeticallly($tutors) {
		#sort tutors by last name then first name
		for($i=0; $i<sizeof($tutors); $i++) {
			for($j=sizeof($tutors)-1; $j>$i; $j--) {
				if($this->beforeAlphabetically($tutors[$j]->student, $tutors[$j-1]->student)) {
					$temp = $tutors[$j];
					$tutors[$j] = $tutors[$j-1];
					$tutors[$j-1] = $temp;
				}
			}
		}
		return $tutors;
	}

	private function beforeAlphabetically($tutor1, $tutor2) {
		if(strcmp($tutor1->lname, $tutor2->lname)<0) {
			return true;
		}
		else if(strcmp($tutor1->lname, $tutor2->lname)>0) {
			return false;
		}
		else {
			if(strcmp($tutor1->fname, $tutor2->fname)<0) {
				return true;
			}
			else return false;
		}
	}

	private function getTutorsTaken($session) {
		$tutorIDs = [];
		for($i=0; $i<sizeof($session); $i++) {
			if(!in_array($session[$i]->tutorID, $tutorIDs)) {
				array_push($tutorIDs, $session[$i]->tutorID);
			}
		}
		return $tutorIDs;
	}
}
?>