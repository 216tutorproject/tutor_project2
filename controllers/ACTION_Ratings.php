<?

include_once($_SERVER['DOCUMENT_ROOT']."/tutor_management/models/Rating.php");
include($_SERVER['DOCUMENT_ROOT']."/tutor_management/models/Student.php"); 

session_start();

include($_SERVER['DOCUMENT_ROOT'].'/tutor_management/sessionsHelper.php');

class Action_Ratings {

	public function submitRatingAjax($request) {
		if(!isset($_SESSION['current_student_id']) || !isset($_COOKIE['session_token'])) { 
			header("location: sign_in.php");
	  	}
	  	elseif(!correctUser()) { #checks to make sure that user has a token that corresponds to the current user 
	    	exit("Identification Mismatch. Please sign in again.");
	  	}
	  	else {
	  		$studentID = $_SESSION['current_student_id'];
	  		$tutorID = $_POST['tutorID'];
	  		$score = $_POST['rating'];
	  		$comments = $_POST['comments'];
	  		$date = date("Y-m-d");
	  		$rating = new Rating('', $studentID, $tutorID, $score, $comments, '');
	  		if($rating->save()) {
	  			echo("{\"status\":\"success\",\"tutorID\":\"$tutorID\",\"score\":\"$score\",\"comments\":\"$comments\",\"date\":\"$date\"}");
	  		}
	  		else {
	  			echo("{\"status\":\"failure\",\"reason\":\"Could not save\"}");
	  		}
	  	}

	}

	public function deleteRatingAjax($request) {
		if(!isset($_SESSION['current_student_id']) || !isset($_COOKIE['session_token'])) { 
			header("location: sign_in.php");
	  	}
	  	elseif(!correctUser()) { #checks to make sure that user has a token that corresponds to the current user 
	    	exit("Identification Mismatch. Please sign in again.");
	  	}
	  	else {
	  		$studentID = $_SESSION['current_student_id'];
	  		$tutorID = $_POST['tutorID'];
	  		if(Rating::delete($studentID, $tutorID)) {
	  			echo("{\"status\":\"success\"}");
	  		}
	  		else {
	  			echo("{\"status\":\"failure\"}");
	  		}
	  	}
	}
}



?>
