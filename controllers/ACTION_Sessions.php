<?php

include($_SERVER['DOCUMENT_ROOT']."/tutor_management/models/Student.php");

class Action_Sessions { 

  #"Sessions," in this case, are USER sessions, not tutor session. Might want to think of renaming this class

  public function sign_in($request) {
    session_start();
  	$uni_id = $_POST['uni_id'];
    $student = Student::findByUniId($uni_id); #should probably use a regex in student model to check submitted id
                                              #and protect against sql injection
    $password_submitted = $_POST['password'];
    if($student->pw_digest == md5($password_submitted)) {
    	$random_string = $this->getRandomString();
    	setcookie('session_token', $random_string, time() + (86400 * 30), "/"); #give student a new session cookie
    	if(!($student->setSessionToken($random_string))) { #provide student with a new session token that corresponds to session cookie
    		exit("server error when saving session token");
    	}
        $_SESSION['current_student_id'] = $student->id; 
        $_SESSION['student'] = $student;
    	header("Location: index.php");
    }else {
    	$_SESSION['failed_sigin'] = true;
    	header("Location: views/sign_in.php");
    }  
  }

  public function sign_out($request) {
    session_start();
  	$student = $_SESSION['student'];
		if(!($student->setSessionToken($this->getRandomString()))) { #get rid of old session token digest
    		exit("Server error when saving session token. Your account may be insecure. See a system admins");
    }
    $_SESSION['student'] = NULL;
    $_SESSION['current_student_id'] = NULL;
    unset($_COOKIE['session_token']);
    session_unset();
    session_write_close();
    header("Location: index.php");
  }

  private function getRandomString($length = 8) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $string = '';
    for ($i = 0; $i < $length; $i++) {
      $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }
    return $string;
  }

}

?>