<?php

include($_SERVER['DOCUMENT_ROOT']."/tutor_management/models/Student.php"); 
include( $_SERVER['DOCUMENT_ROOT']."/tutor_management/models/TutorSession.php");

session_start();

include($_SERVER['DOCUMENT_ROOT'].'/tutor_management/sessionsHelper.php');

class Action_TutorSessions {

	public function addSessionAjax($request) {
	  if(!isset($_SESSION['current_student_id']) || !isset($_COOKIE['session_token'])) { 
		header("location: sign_in.php");
	  }
	  elseif(!correctUser()) { #checks to make sure that user has a token that corresponds to the current user 
	    exit("Identification Mismatch. Please sign in again.");
	  }
	  else {
	  	$studentID = $_SESSION['current_student_id'];
	  	$student = Student::findById($studentID);
	  	$tutor = $student->getTutorIfTutor(); #returns false if student is not a tutor, else it returns a tutor object corresponding to student
	  	if($tutor) {
	  		$tutor_id = $tutor->id;
	  		$building = $request->get('building');
	  		$room = $request->get('room_num');
	  		$tutorSession = new TutorSession('', $tutor_id, '', $building, $room);
	  		if($student->isAvailable() && $tutorSession->save($tutor)) { #saves tutor session object to database if tutor object is valid
	  			$html = TutorSession::getCurrentSession($tutor->id)->toHtmlCurrent();
				echo("{\"status\":\"Session Added\", \"html\":\"$html\"}"); 
			}
			else {
				echo("{\"status\":\"Failure: You are already registered for a current tutor session\"}"); 
			}
	  	}
	  	else {
	  		echo("{\"status\":\"Failure: You are not a tutor\"}");
	  	}
	  }
	}

	public function joinSessionAjax($request) {
	  if(!isset($_SESSION['current_student_id']) || !isset($_COOKIE['session_token'])) { 
		header("location: sign_in.php");
	  }
	  elseif(!correctUser()) { #checks to make sure that user has a token that corresponds to the current user 
	    exit("Identification Mismatch. Please sign in again.");
	  }
	  else {
	  	$student = Student::findByID($_SESSION['current_student_id']);
	  	if(!$student->isAvailable()) {
	  		echo("{\"status\":\"failure\",\"reason\":\"Could not add student to session. $student->fname $student->lname is already registered for a current session as a tutor\"}");
	  	}
	  	else {
	  		$sessionID = $request->get('sessionID');
	  		if($student->joinSession($sessionID)) {
	  			$session = TutorSession::findByID($sessionID);
	  			$html = $session->joinSessionHTMLcurrent();
	  			echo("{\"status\":\"success\",\"html\":\"$html\"}");
	  		}
	  		else{
	  			echo("{\"status\":\"failure\",\"reason\":\"failed to save attendance\"}");
	  		}
	  	}
	  }
	}
}

?>