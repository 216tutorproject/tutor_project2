<?php

include_once 'Request.php';

$request = new Request();
$class = 'Action_' . $request->getGroup();
require_once 'controllers/'.$class . '.php';
$action = new $class;
$cmd = $request->getCommand();
$action->$cmd($request);

?>
