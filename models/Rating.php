<?

class Rating {
	public $id;
	public $student_id;
	public $tutor_id;
	public $score;
	public $comments;
	public $datetime;

	function __construct($id, $student_id, $tutor_id, $score, $comments, $datetime) {
        if($id != "") {
            $this->id = $id;
        }       
        $this->student_id = $student_id;
        $this->tutor_id = $tutor_id;
        $this->score= $score;
        $this->comments = $comments;
        if($datetime != '') {
            $this->datetime = $datetime;
        }
    }

    public function save() {
        $dao = RatingDAO::getRatingDAO();
        $hasRated = $dao->studentHasRated($this);
        $hasAttended = $dao->studentHasAttended($this);
        if(!$hasRated && $hasAttended) {
            if($dao->insertRating($this)) {
                return true;
            }
        }
        return false;
    }

    public static function delete($studentID, $tutorID) {
        $dao = RatingDAO::getRatingDAO();
        if($dao->deleteRating($studentID, $tutorID)) {
            return true;
        }
        else return false;
    }

    public function date() {
        if(!isset($this->datetime)) {
            return null;
        }
        else {
            return substr($this->datetime, 0, 10);
        }
    }
}

class RatingDAO {

    public static $dao;

    function __construct() {        
    }

    function __destruct() {
    }

    public static function getRatingDAO() {
        if(isset(RatingDAO::$dao)) {
            return RatingDAO::$dao;
        }
        else {
            RatingDAO::$dao = new RatingDAO();
            return RatingDAO::$dao; 
        }
    }

    private function getDBConnection() {
        if (!isset($_mysqli)) {
            $_mysqli = new mysqli("localhost", "root", "", "tutor_database");
            if ($_mysqli->errno) {
                printf("Unable to connect: %s", $_mysqli->error);
                exit();
            }
        }
        return $_mysqli;
    }

    public function deleteRating($studentID, $tutorID) {
        $con = $this->getDBConnection();
        if(mysqli_query($con,"DELETE FROM rates WHERE student_id=$studentID AND tutor_id=$tutorID")) {
            return true;
        }
        else return false;
    }

    public function insertRating($rating) {
        $con = $this->getDBConnection();
        $studentID = $rating->student_id;
        $tutorID = $rating->tutor_id;
        $score = $rating->score;
        $comments = $rating->comments;
        if(mysqli_query($con,"INSERT INTO rates (student_id, tutor_id, score, comments, date_time) VALUES ($studentID, $tutorID, $score, '$comments"."', NOW())")) {
            return true;
        } else {
            return false;
        }
    }

    public function studentHasRated($rating) {
        $con = $this->getDBConnection();
        $result = $con->query("SELECT id FROM rates WHERE student_id = $rating->student_id AND tutor_id=$rating->tutor_id");
        if($result) {
            for($i=0; $result->fetch_row(); $i++) {
                return true;
            }
            return false;
        }
        else {exit("Database Error: RateDAO::studentHasRated"); }
    }

    public function studentHasAttended($rating) {
        $con = $this->getDBConnection();
        $result = $con->query("SELECT id FROM `session` WHERE tutor_id = $rating->tutor_id");
        if($result) {
            $id = '';
            $i = 0;
            while($row = $result->fetch_row()) {
                $id = $row[0];
                $result2 = $con->query("SELECT id FROM attends WHERE session_id = $id and student_id = $rating->student_id");
                while($result2->fetch_row()) {
                    return true;
                }
            }
            return false;
        }
        else {exit("Database Error: RateDAO::studentHasAttended"); }
    }
}

?>





