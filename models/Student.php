<?php

include_once "Tutor.php";

class Student {
	public $id;
    public $fname;
    public $lname;
    public $uni_id;
    public $pw_digest;
    public $session_token;

    function __construct($id, $fname, $lname, $uni_id, $pw_digest, $session_token) {
        if($id != "") {
            $this->id = $id;
        }       
        $this->fname = $fname;
        $this->lname = $lname;
        $this->uni_id= $uni_id;
        $this->pw_digest = $pw_digest;
        $this->session_token = $session_token;
    }

    public static function findByID($id) {
        $student_dao = new StudentDAO();
        return $student_dao->getStudentByPrimaryKey($id);
    }

    public static function findByUniID($uni_id) {
        $student_dao = new StudentDAO();
        return $student_dao->getStudentByUniId($uni_id);
    }

    public static function findBySessionToken($token) {
        $student_dao = new StudentDAO();
        return $student_dao->getStudentBySessionToken($token);
    }

    public function setSessionToken($token) {
        $student_dao = new StudentDAO();
        return $student_dao->setSessionToken($this, $token);
    }

    public function getTutorIfTutor() {
        $tutor_dao = new TutorDAO();
        $tutor = $tutor_dao->getTutorByStudentID($this->id);
        if($tutor != NULL) {
            return $tutor;
        } else {
            return false;
        }
    }

    public static function getAllTutors() { ########################################################
        $dao = new TutorDAO();
        $tutors = $dao->selectAllTutors();
        return $tutors;
        #for each tutor, get there reviews
        #return the list of tutors
    }

    public function getCurrentSessionID() {
        $dao = new StudentDAO();
        return $dao->currentSession($this->id);
    }

    public function isAvailable() {
        $dao = new StudentDAO();
        if($dao->currentSession($this->id)) {
            return false;
        }
        $tutor = $this->getTutorIfTutor();
        if($tutor) {
            if(!$tutor->isAvailable()) {
                return false;
            }
        }
        return true;
    } 

    public function joinSession($sessionID) {
        $dao = new StudentDAO();
        if($dao->insertAttendance($this->id, $sessionID)) {
            return true;
        }
        else {
            return false;
        }
    } 

    public function getPastRatings($pastTutorIDs) {
        $dao = new StudentDAO();
        $reviewArray = [];
        for($i=0; $i<sizeof($pastTutorIDs); $i++) {
            $review = $dao->selectRatingsByRaterRatee($this->id, $pastTutorIDs[$i]);
            $reviewArray[$pastTutorIDs[$i]] = $review;
        }
        return $reviewArray;
    }
}

class StudentDAO {

    function __construct() {        
    }

    function __destruct() {
    }

    private function getDBConnection() {
        if (!isset($_mysqli)) {
            $_mysqli = new mysqli("localhost", "root", "", "tutor_database");
            if ($_mysqli->errno) {
                printf("Unable to connect: %s", $_mysqli->error);
                exit();
            }
        }
        return $_mysqli;
    }

    public function selectRatingsByRaterRatee($studentID, $tutorID) {
        $con = $this->getDBConnection();
        $result = $con->query("SELECT id, score, comments, date_time FROM rates WHERE student_id=$studentID and tutor_id = $tutorID");
        if($result) {
            while($row = $result->fetch_row()) {
                $rating = new Rating($row[0], $studentID, $tutorID, intval($row[1]), $row[2], $row[3]);
                return $rating;
            }
            return false;
        }
        else {exit("DB problem StudentDAO::selectRatingsByRaterRatee");}
    }

    public function currentSession($id) {
        $con = $this->getDBConnection();
        $result = $con->query("SELECT session_id FROM attends WHERE student_id=$id");
        if($result==false) {
            exit("Database Error: Cannot query 'attends' table in StudentDAO::isAttendingSession");
        }
        $i = 0;
        $session = NULL;
        while($row = $result->fetch_row()) {
            $sessionID = $row[0];
            $result1 = $con->query("SELECT id FROM `session` WHERE id=$sessionID AND date_time > NOW() - INTERVAL 2 HOUR");
            $row1 = $result1->fetch_row();
            if(sizeof($row1[0]) > 0) {
                return $row1[0];
            }
        }
        return false;
    }

    public function getAllStudents() {
        $con = $this->getDBConnection();
        $result = $con->query("SELECT id, fname, lname, uni_id, pw_digest, session_token FROM student");
        $i = 0;
        while ($row = $result->fetch_row()) {
            $rec = new Student($row[0], $row[1], $row[2], $row[3], $row[4], $row[5]);
            $lst[$i++] = $rec;
        }
        return $lst;
    }

    public function getStudentByPrimaryKey($id) {
        $con = $this->getDBConnection();
        $result = $con->query("SELECT id, fname, lname, uni_id, pw_digest, session_token FROM student WHERE id=$id");
        if($result != false) {
            $row = $result->fetch_row();
            if(sizeof($row[0]) > 0) {
                $student = new Student($row[0], $row[1], $row[2], $row[3], $row[4], $row[5]);
                return $student;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }   
    }

    public function getStudentByUniID($uni_id) {
        $con = $this->getDBConnection();
        $result = $con->query("SELECT id, fname, lname, uni_id, pw_digest, session_token FROM student WHERE uni_id='$uni_id'");
        if($result != false) {
            $row = $result->fetch_row();
            if(sizeof($row[0]) > 0) {
                $student = new Student($row[0], $row[1], $row[2], $row[3], $row[4], $row[5]);
                return $student;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }   
    }

    public function getStudentBySessionToken($session_token) {
        $con = $this->getDBConnection();
        $hashed_token = md5($session_token);
        $result = $con->query("SELECT id, fname, lname, uni_id, pw_digest, session_token FROM student WHERE session_token='$hashed_token'");
        if($result != false) {
            $row = $result->fetch_row();
            if(sizeof($row[0]) > 0) {
                $student = new Student($row[0], $row[1], $row[2], $row[3], $row[4], $row[5]);
                return $student;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }   
    }

    public function getPasswordDigest($student) {
        $con = $this->getDBConnection();
        $result = $con->query("SELECT pw_digest FROM student WHERE id=$student->id");
        if($result != false) {
            $row = $result->fetch_row();
            $digest = $row[0];
            return $digest;
        } else {
            return NULL;
        }
    }

    public function setSessionToken($student, $str) {
        $con = $this->getDBConnection();
        $new_token = md5($str);
        $success = mysqli_query($con,"UPDATE student SET session_token = '$new_token' WHERE id = $student->id");
        if($success) {
            return true;
        }
        else return false;
    }

    public function insertAttendance($studentID, $sessionID) {
        $con = $this->getDBConnection();
        if(mysqli_query($con,"INSERT INTO attends (student_id, session_id) VALUES ($studentID, $sessionID)")) {
            return true;
        } else {
            return false;
        }
    }

    #for population:

    public function countStudents() {
        $con = $this->getDBConnection();
        $result = $con->query("SELECT id FROM student");
        $i = 0;
        while ($row = $result->fetch_row()) {
            $i++;
        }
        return $i;
    }

    public function saveStudent($student) {
        $con = $this->getDBConnection();
        $fname = $student->fname;
        $lname = $student->lname;
        $uni_id = $student->uni_id;
        $pw_digest = $student->pw_digest;
        $hashed_token = md5($Student->session_token);
        if(mysqli_query($con,"INSERT INTO student (fname, lname, uni_id, pw_digest, session_token) VALUES ('$fname', '$lname', '$uni_id', '$pw_digest', 'session_token')")) {
            return true;
        } else {
            return false;
        }
    }
 }

?>