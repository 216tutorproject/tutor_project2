<?php

include_once "Rating.php";

class tutor {
  #in "tutors table"
	public $id;
  public $student_id;

  #in "rates" join table
  public $ratings;
  public $avgScore=0;

  #should have used inheritance. Instead used a pointer to corresponding student information
  public $student;

  function __construct($id, $student_id) {
    if($id != "") {
      $this->id = $id;
    }       
    $this->student_id = $student_id;
  }

  public function isAvailable() {
    $dao = new TutorDAO();
    if($dao->hasCurrentSession($this->id)) {
      return false;
    }
    else return true;
  }

  public function gatherRatingsData() {
    $dao = new TutorDAO();
    $this->ratings = $dao->getRatings($this->id);
    $this->aggregateData();
  } 

  public function aggregateData() {
    $sum = 0;
    $count = sizeof($this->ratings);
    for($i=0; $i<$count; $i++) {
      $sum = $sum + $this->ratings[$i]->score;
    }
    if($count>0) {
      $this->avgScore = $sum/$count;
    }
  }  
}

class TutorDAO {

	function __construct() {        
	}

  function __destruct() {
  }

    private function getDBConnection() {
        if (!isset($_mysqli)) {
            $_mysqli = new mysqli("localhost", "root", "", "tutor_database");
            if ($_mysqli->errno) {
                printf("Unable to connect: %s", $_mysqli->error);
                exit();
            }
        }
        return $_mysqli;
    }

    public function getTutorByPrimaryKey($id) {
    	$con = $this->getDBConnection();
    	$result = $con->query("SELECT id, student_id FROM tutor WHERE id=$id");
      if($result != false) {
        $row = $result->fetch_row();
        if(sizeof($row[0])>0) {
          $tutor = new Tutor($row[0], $row[1]);
          return $tutor;
        } else {
          return NULL;
        }
      } else {
        return NULL;
      }
   	}

    public function selectAllTutors() {
        $con = $this->getDBConnection();
        $result = $con->query("SELECT id, student_id FROM tutor");
        $tutors = []; $i = 0;
        while ($row = $result->fetch_row()) {
            $rec = new Tutor($row[0], $row[1]);
            $tutors[$i++] = $rec;
        }
        return $tutors;
    }

   	public function getTutorByStudentId($id) {
      $con = $this->getDBConnection();
      $result = $con->query("SELECT id, student_id FROM tutor WHERE student_id=$id");
      if($result != false) {
        $row = $result->fetch_row();
        if(sizeof($row[0])>0) {
          $tutor = new Tutor($row[0], $row[1]);
          return $tutor;
        } else {
          return NULL;
        }
      } else {
        return NULL;
      }
    }

    public function getRatings($tutorID) {
      $ratings=[]; $con = $this->getDBConnection();
      $result = $con->query("SELECT id, student_id, score, comments, date_time FROM rates WHERE tutor_id = $tutorID");
      if($result) {
        $i = 0;
        $rating = null;
        while ($row = $result->fetch_row()) {
          $rating = new Rating($row[0], $row[1], $tutorID, intval($row[2]), $row[3], $row[4]);
          $ratings[$i++] = $rating;
        }
        return $ratings;
      }
      else {exit("DB problem in TutorDAO::getRatings");}
    }

   	public function createTutor($student) {
   		$con = $this->getDBConnection();
   		if ($this->getTutorByStudentId($student->id) == NULL) {
   			$student_id = $student->id;
   			$success = mysqli_query($con,"INSERT INTO tutor (student_id) VALUES ('$student_id')");
   			if($success) {
   				return true;
   			}
   		}
   		return false;
   	}

   	public function deleteTutor($tutor) {
   		$con = $this->getDBConnection();
   		$id = $tutor->id;
   		$success = mysqli_query($con,"DELETE FROM tutor  WHERE id=$id");
   		if($success) {
   			return true;
   		}
   		else return false;
   	}

    public function hasCurrentSession($id) {
      $con = $this->getDBConnection();
      $result = $con->query("SELECT id, date_time, building, room_num FROM `session` WHERE date_time > NOW() - INTERVAL 2 HOUR AND tutor_id=$id ORDER BY `date_time` DESC");
      $row = $result->fetch_row();
      if($result==false) {
        exit("Database Error: Cannot query tutor table in TutorSessionDAO::getCurrentSession");
      }
      else if(sizeof($row[0]) > 0) {
        return true;
      }
      else {
        return false;
      }
    }



    #for db population:

    public static function countTutors() {
      $con = $this->getDBConnection();
      $result = $con->query("SELECT id FROM tutor");
      $i = 0;
        while ($row = $result->fetch_row()) {
            $i++;
        }
        return $i;
    }

}

?>