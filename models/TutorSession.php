<?php

class tutorSession {
	public $id;
    public $tutorID;
    public $datetime;
    public $building;
    public $roomNum;

    function __construct($id, $tutorID, $datetime, $building, $room) {
        if($id != "") {
            $this->id = $id;
        }       
        $this->tutorID = $tutorID;
        $this->datetime = $datetime;
        $this->building = $building;
        $this->roomNum = $room;
    }

    public static function findByID($id) {
        $dao = new TutorSessionDAO();
        return $dao->selectTutorSessionByID($id);
    }

    public static function getActiveSessions() {
        $dao = new TutorSessionDAO();
        return $dao->selectActiveSessions();
    }

    public static function getCurrentSession($id) {
        $dao = new TutorSessionDAO();
        return $dao->getCurrentSession($id);
    }

    public static function getPastSessions($id) {
        $dao = new TutorSessionDAO();
        return $dao->getPastSessions($id);
    }

    public static function getPastTuteeSessions($id) {
        $dao = new TutorSessionDAO();
        $ids = $dao->selectPastTuteeSessionIDs($id);
        $sessions = [];
        $i=0;
        foreach ($ids as $id) {
            $sessions[$i++] = $dao->selectTutorSessionByID($id);
        }
        return $sessions;
    }

    public function save($tutor) {
        if(!$tutor->isAvailable()) {
            return false;
        }
    	$tutorSessionDAO = new TutorSessionDAO();
    	return $tutorSessionDAO->saveTutorSession($this);
    }

    public function toHtmlCurrent() {
        return "<div class='session' session_id='".$this->id."'>".
            "Current Session in ".$this->building." ".$this->roomNum." <br>".
            "Started at ".$this->startTime()." ends at ".$this->endTime()."</div>";
    }

    public function toHtmlPast() {
        return "<div class='session' session_id='".$this->id."'>".
            $this->date()." ".$this->startTime()." to ". $this->endTime()." in ".
            $this->building." ".$this->roomNum."</div>";
    }

    public function joinSessionHTML() {
        $dao =  new TutorSessionDAO();
        $name = $dao->selectTutorNameBySession($this);
        return "<div class= 'join-session'> <a class='join-link' id='$this->id'>Session $this->id</a> </br>Tutor: $name </br>Time: ".
        $this->startTime()." to ".$this->endTime()."</br> Location: ".$this->building." ".$this->roomNum."</div>";
    }

    public function joinSessionHTMLcurrent() {
        $dao =  new TutorSessionDAO();
        $name = $dao->selectTutorNameBySession($this);
        return "<div class= 'join-session' id='$this->id'> Tutor: $name </br>Time: ".
        $this->startTime()." to ".$this->endTime()."</br> Location: ".$this->building." ".$this->roomNum."</div>";
    }

    public function pastHTML() {
        $dao =  new TutorSessionDAO();
        $date = $this->date();
        $name = $dao->selectTutorNameBySession($this);
        return "<div class= 'past-session'> Session: $this->id </br>Tutor: $name </br> Date: $date </br> Time: ".
        $this->startTime()." to ".$this->endTime()."</br> Location: ".$this->building." ".$this->roomNum."</div>";
    }

    public function endTime() {
        if(!isset($this->datetime)) {
            return null;
        }
        else {
            $time=substr($this->datetime, -8);
            $endHour = (intval(substr($time, 0, 2)) + 2)%24;
            $endTime = (String)$endHour.substr($time, -6, 3);
            return $endTime;
        }
    }

    public function startTime() {
        if(!isset($this->datetime)) {
            return null;
        }
        else {
            return substr($this->datetime, -8, 5);
        }
    }

    public function date() {
        if(!isset($this->datetime)) {
            return null;
        }
        else {
            return substr($this->datetime, 0, 10);
        }
    } 
}

class TutorSessionDAO {

    function __construct() {        
    }

    function __destruct() {
    }

    private function getDBConnection() {
        if (!isset($_mysqli)) {
            $_mysqli = new mysqli("localhost", "root", "", "tutor_database");
            if ($_mysqli->errno) {
                printf("Unable to connect: %s", $_mysqli->error);
                exit();
            }
        }
        return $_mysqli;
    }

    public function saveTutorSession($tutorSession) {
        $con = $this->getDBConnection();
        $tutor_id = $tutorSession->tutorID;
        $building = $tutorSession->building;
        $room = $tutorSession->roomNum;
        if(mysqli_query($con,"INSERT INTO session (tutor_id, date_time, building, room_num) VALUES ($tutor_id, NOW(), '$building', '$room')")) {
            return true;
        } else {
            return false;
        }
    }

    public function selectTutorSessionByID($id) {
        $con = $this->getDBConnection();
        $result = $con->query("SELECT tutor_id, date_time, building, room_num FROM `session` WHERE id=$id");
        if($result != false) {
            $row = $result->fetch_row();
            if(sizeof($row[0])>0) {
                $session = new TutorSession($id, $row[0], $row[1], $row[2], $row[3]);
            return $session;
        } else {
          return false; ######
        }
      } else {
        return false; ########
      }
    }

    public function selectActiveSessions() {
        $con = $this->getDBConnection();
        $result = $con->query("SELECT id, tutor_id, date_time, building, room_num FROM `session` WHERE date_time > NOW() - INTERVAL 2 HOUR ORDER BY `date_time` DESC");
        if($result) {
            $sessions = [];
            $i = 0;
            while ($row = $result->fetch_row()) {
                $rec = new TutorSession($row[0], $row[1], $row[2], $row[3], $row[4]);
                $sessions[$i++] = $rec;
            }
            return $sessions;
        }
        else {
            exit("Database Error: Cannot query tutor session table in TutorSessionDAO::selectActiveSessions");
        }
    }

    public function getCurrentSession($id) {
      $con = $this->getDBConnection();
      $result = $con->query("SELECT id, date_time, building, room_num FROM `session` WHERE date_time > NOW() - INTERVAL 2 HOUR AND tutor_id=$id ORDER BY `date_time` DESC");
      $row = $result->fetch_row();
      if($result==false) {
        exit("Database Error: Cannot query tutor session table in TutorSessionDAO::getCurrentSession");
      }
      else if(sizeof($row[0]) > 0) {
        $session = new TutorSession($row[0], $id, $row[1], $row[2], $row[3]);
        return $session;
      }
      else {
        return false;
      }
    }

    public function selectTutorNameBySession($session) {
      $con = $this->getDBConnection();
      $result = $con->query("SELECT tutor_id FROM `session` WHERE id=$session->id");
      if(!$result) {exit("Database error in TutorSessionDAO::selectTutorNameBySession");}
      $tutorID = $result->fetch_row()[0];
      $result = $con->query("SELECT student_id FROM tutor WHERE id = $tutorID");
      if(!$result) { exit("Database error in TutorSessionDAO::selectTutorNameBySession");}
      $id = $result->fetch_row()[0];
      $result = $con->query("SELECT fname, lname FROM student WHERE id = $id");
      if(!$result) { exit("Database error in TutorSessionDAO::selectTutorNameBySession");}
      $row = $result->fetch_row();
      $name = $row[0]." ".$row[1];
      return $name;
    }

    public function getPastSessions($id) {
      $con = $this->getDBConnection();
      $sessions = [];
      $result = $con->query("SELECT id, date_time, building, room_num FROM `session` WHERE date_time < NOW() - INTERVAL 2 HOUR AND tutor_id=$id ORDER BY `date_time` DESC");
      if($result) {
        $i = 0;
        while ($row = $result->fetch_row()) {
          $rec = new TutorSession($row[0], $id, $row[1], $row[2], $row[3]);
          $sessions[$i++] = $rec;
        }
        return $sessions;
      }
      else {
        exit("Database Error: Cannot query tutor table in TutorSessionDAO::getPastSessions");
      }
    }

    public function selectPastTuteeSessionIDs($id) {
      $con = $this->getDBConnection();
      $sessionIDs = [];
      $result = $con->query("SELECT session_id FROM attends WHERE student_id = $id");
      if($result) {
        $i=0;
        while($row=$result->fetch_row()) {
            $sessionIDs[$i++] = $row[0];
        }
        return $sessionIDs;
      }
      else {
        exit("Database Error: Cannot query tutor table in TutorSessionDAO::selectPastTuteeSessionIDs");
      }
    }

}

?>