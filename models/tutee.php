<?php

class tutee {
    public $id;
    public $student_id;

    function __construct($id, $student_id) {
        if($id != "") {
            $this->id = $id;
        }       
        $this->student_id = $student_id;
    }
    
}

class TuteeDAO {

	function __construct() {        
	}

  function __destruct() {
  }

  private function getDBConnection() {
    if (!isset($_mysqli)) {
      $_mysqli = new mysqli("localhost", "root", "", "tutor_database");
      if ($_mysqli->errno) {
        printf("Unable to connect: %s", $_mysqli->error);
        exit();
      }
    }
    return $_mysqli;
  }

  public function getTuteeByPrimaryKey($id) {
      $con = $this->getDBConnection();
      $result = $con->query("SELECT id, student_id FROM tutee WHERE id=$id");
      if($result != false) {
        $row = $result->fetch_row();
        if(sizeof($row[0])>0) {
          $tutor = new Tutee($row[0], $row[1]);
          return $tutor;
        } else {
          return null;
        }
      } else {
        return null;
      }
    }

  public function getTuteeByStudentId($id) {
    $con = $this->getDBConnection();
    $result = $con->query("SELECT id, student_id FROM tutee WHERE student_id=$id");
    if($result != false) {
      $row = $result->fetch_row();
      if(sizeof($row[0])>0) {
        $tutor = new Tutee($row[0], $row[1]);
        return $tutor;
      } else {
          return null;
      }
    } else {
        return null;
    }
  }

  public function createTutee($student) {
   	$con = $this->getDBConnection();
   	if ($this->getTuteeByStudentId($student->id) == NULL) {
   		$student_id = $student->id;
   		$success = mysqli_query($con,"INSERT INTO tutee (student_id) VALUES ('$student_id')");
   		if($success) {
   			return true;
   		}
   	}
   	return false;
  }

  public function deleteTutee($tutor) {
   	$con = $this->getDBConnection();
   	$id = $tutor->id;
   	$success = mysqli_query($con,"DELETE FROM tutee WHERE id=$id");
   	if($success) {
   		return true;
   	}
   	return false;
  }

  #for populate function

  public function countTutees() {
    $con = $this->getDBConnection();
    $result = $con->query("SELECT id FROM tutee");
    $i = 0;
    while ($row = $result->fetch_row()) {
      $i++;
    }
    return $i;
  }

}

?>