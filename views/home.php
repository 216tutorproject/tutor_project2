<!DOCTYPE html>

<?php

include($_SERVER['DOCUMENT_ROOT']."/tutor_management/models/Student.php");
include($_SERVER['DOCUMENT_ROOT']."/tutor_management/models/TutorSession.php");
include($_SERVER['DOCUMENT_ROOT']."/tutor_management/assets/bootstrap/source.php");

session_start();

###If the user reloads the page we want to go back through the controller in case data has changed

if(isset($_SESSION['refreshFlag']) && !is_null($_SESSION['refreshFlag'])) {
	$_SESSION['refreshFlag'] = null;
	header("Location: ../index.php");
}
else {
	$_SESSION['refreshFlag'] = true;
}

##make sure the student is logged in, then assign appropraite variables

if(!isset($_SESSION['student'])) {
	exit("Not logged in");
}
else {
	$student = $_SESSION['student'];
	$activeSessions = $_SESSION['activeSessions'];
	$currentTuteeSession = $_SESSION["currentTuteeSession"];
	$pastTuteeSessions = $_SESSION['pastTuteeSessions'];
	$pastTutorIDs = $_SESSION['pastTutorIDs'];
	$pastTutorReviews = $_SESSION['pastTutorReviews'];
	$tutorList = $_SESSION['tutorList'];
	if($_SESSION['tutor']) {
		include($_SERVER['DOCUMENT_ROOT']."/tutor_management/views/startSessionForm.php");
		$tutor = $_SESSION['tutor'];
		$currentTutorSession = $_SESSION['currentTutorSession'];
		$pastTutorSessions = $_SESSION['pastTutorSessions'];
	}
	else {
		$tutor = false;
		$currentTutorSession = false;
	}
}

?>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Student Home Page</title>
    <script type="text/javascript" src="../assets/scripts/jquery-1.11.1.min.js"></script>        
    <script type="text/javascript" src="../assets/scripts/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="../assets/scripts/home.js"></script> 
    <link rel="stylesheet" type="text/css" href="../assets/stylesheets/customStyles.css"></style>
    <? echo $bootstrapSource; ?>
</head>

<body class = "brown-background">

<?
#if(isset($pastTutorReviews[4])) {
#echo $pastTutorReviews[4]->score;
#}
?>

<div class="container main-container white-background">

<div id="main-navbar" class = "navbar">
	<div class="row">
		<div id="navbar-name-area"class = "col-lg-6 col-md-6 col-sm-5 col-xs-12 center-xs">
			<h1 id="navbar-name">TutorAid</h1>
		</div>
		<div class = "col-lg-5 col-md-4 col-sm-5 col-xs-12 right-text center-xs">
			<button class="btn btn-primary" id="toggle-tutee-mode"> Tutee</button>
			<? if($tutor) { echo("<button class = 'btn btn-primary' id='toggle-tutor-mode'> Tutor</button>");} ?>
			<button class="btn btn-primary" id="profile-btn"> Profile</button>
			<button class="btn btn-primary" id="about-btn"> About</button>
			<button class="btn btn-primary" id="help-btn"> Help</button>
		</div>
		<div class = "col-lg-1 col-md-2 col-sm-2 col-xs-12 center-xs">
			<form id="sign-out-button" method="post" action='../index.php'>
				<input type="hidden" name='grp' value='Sessions'/>
				<input type="hidden" name="cmd" value="sign_out"/>
				<input id="sign-out-submit" class="btn btn-primary center-xs" type="submit" name="btn" value="Sign Out"></input>
			</form>
		</div>
	</div>
</div>

<div id="tutee-section" class="row col-md-8 col-md-offset-2">
	<ul id="tutee-navigation" class="nav nav-tabs mid-blue-link">
		<li> 
			<a href="#tutee_current_sessions" data-toggle="tab"> Current Sessions
				<span class="badge"> <? echo sizeof($activeSessions)?> </span>
			</a>
		</li>
		<li> 
			<a href="#tutee_past_sessions" data-toggle="tab">Session History
				<span class="badge"><? echo sizeof($pastTuteeSessions)?></span>
			</a> 
		</li>
		<li> 
			<a href="#tutee_messages" data-toggle="tab">Messages
				<span class="badge"><? echo "0"?></span>
			</a> 
		</li>
		<li> 
			<a href="#tutor_index" data-toggle="tab">Tutor Profiles</a> 
		</li>
	</ul>

	<div class="container-fluid">
		<div class="row">
			<div id="tutee-tabs-content" class="col-md-12 tab-content">
				<div class="tab-pane" id="tutee_current_sessions">
					<div id="current-tutee-session" class="row">
						<?	
							if($currentTuteeSession) {
								echo "<p>You are currently attending this session: </p>";
								echo $currentTuteeSession->joinSessionHtmlCurrent();
							}
						?>
					</div>
					<div id="active-tutor-sessions"class="row">
						<?
							if(!$currentTuteeSession) {
								if(sizeof($activeSessions)>0) {
									echo("<p>Current Sessions: Click to Join!</p>");
									for($i=0; $i<sizeof($activeSessions); $i++) {
										echo($activeSessions[$i]->joinSessionHTMl());
									}
								}
								else { 
									echo("<p>There are no current sessions. Please contact the Office of Academic Success
									to find out when sessions will be held");
								}	
							}
						?>
					</div>
				</div>
				<div class="tab-pane" id="tutee_past_sessions">
					<div class="row">
						<?php
							if(sizeof($pastTuteeSessions) < 1) {
								echo "<p>You have not yet attended a tutor session<p>";
							}
							else {
								for($i=0; $i<sizeof($pastTuteeSessions); $i++) {
									echo $pastTuteeSessions[$i]->pastHTMl();
								}
							}
						?>
					</div>
				</div>
				<div class="tab-pane" id="tutee_messages">
					<h1>Messages to Come</h1>
				</div>
				<div class="tab-pane" id="tutor_index">
					<?
						$colors = ["gray", "white"];
						$i=0;
						$had=false;
						$reviewed=false;
						foreach($tutorList as $tutor) {
							if(in_array(intval($tutor->id), $pastTutorIDs)) {
								$had = true;
								if($pastTutorReviews[intval($tutor->id)]) {
									$reviewed=true;
								}
								else $reviewed=false;
							}
							else { $had=false; $reviewed = false;}
							$color = $colors[$i++ % 2] . "-background";
							echo "<div id='$tutor->id' class='row $color'> <div id='tutor-buttons-$tutor->id' class='col-md-2'>";
							$name = $tutor->student->fname . " " . $tutor->student->lname; 
							echo "<p class='tutor-name'> <strong> $name </strong> </p>";
							echo "<button id ='view-tutor".$tutor->id."' tutor='$tutor->id' class='view-tutor btn btn-info'> View Tutor Profile </button>";
							echo "<button id ='hide-tutor".$tutor->id."' tutor='$tutor->id' class='hide-tutor btn btn-info hidden'> Hide Tutor Profile </button>";

							if($had && !$reviewed) {
								echo "<button id ='rate-tutor".$tutor->id."-btn' tutor='$tutor->id' class='rate-tutor btn btn-info'> Rate this tutor </button>";
								echo "<button id ='hide-form".$tutor->id."' tutor='$tutor->id' class='hide-form btn btn-info hidden'> Hide Form </button>";
							}

							if($reviewed) {
								echo "<button id ='view-review".$tutor->id."-btn' tutor='$tutor->id' class='view-review btn btn-info'> View Your Rating </button>";
								echo "<button id ='hide-review".$tutor->id."' tutor='$tutor->id' class='hide-review btn btn-info hidden'> Hide Your Rating </button>";
							}
							echo "</div>";

							echo "<div id='tutor".$tutor->id."-info' class = 'hidden col-md-10'>";
							
							echo "<div id='tutor".$tutor->id."-data' class = 'row col-md-12'>";
							echo "Ratings: ".sizeof($tutor->ratings)." Avg: ".$tutor->avgScore."</div>";
							echo "<table id='".$tutor->id."-table' class = 'table table-striped'> <thead> <tr> <th class='tutor-table-cat'> Date </th> <th class='tutor-table-cat'> Score </th> <th class='tutor-table-cat'> Comments</th> </tr> </thead>";
							echo "<tbody>";
							for($j=0; $j<sizeof($tutor->ratings); $j++) {
								$rating = $tutor->ratings[$j];
 								echo "<tr><td>".$rating->date()."</td> <td> $rating->score </td> <td> $rating->comments </td></tr>";
 							}
 							echo "</tbody>";
							echo "</table>";
							echo "</div>";
							
							echo "<div id='rate-tutor".$tutor->id."' class='row rate-area hidden'>";
							echo "<div class = 'col-md-3'>";
							echo "<select id='tutor".$tutor->id."-rate' class='rate-select'> <option value='1'>1</option> <option value='2'>2</option> <option value='3'>3</option> <option value='4'>4</option> <option value='5'>5</option></select><span class='rating-label'> Rate (1-5)</span></div>";
							echo "<div id='tutor".$tutor->id."-comments' class='col-md-4'> <textarea id='tutor".$tutor->id."-comments-input' class='form-control' placeholder='Comments'></textarea></div>";
							echo "<div class='col-md-2'>";
							echo "<button id ='submit-rating".$tutor->id."' tutor='$tutor->id' class='submit-rating btn btn-primary'> Submit </button>";
							echo "</div>";
							echo "</div>";

							if($reviewed) {
								$ownRating = $pastTutorReviews[intval($tutor->id)];
								$date = $ownRating->date();
							}
							else {
								$ownRating = "";
								$date = "";
							}
							echo "<div id='view-own-rating".$tutor->id."' tutor='$tutor->id' class = 'hidden col-md-10 own center-text'>";
							echo "<table id='".$tutor->id."-own-table' class = 'table table-striped text-left'> <thead> <tr> <th class='tutor-table-cat'> Date </th> <th class='tutor-table-cat'> Score </th> <th class='tutor-table-cat'> Comments</th> </tr> </thead>";
							echo "<tbody>";
							echo "<tr><td id='date-$tutor->id'>".$date."</td> <td id='score-$tutor->id'> $ownRating->score </td> <td id='comments-$tutor->id'> $ownRating->comments </td></tr>";
							echo "</tbody>";
							echo "</table>";
							echo "<button id ='delete-tutor".$tutor->id."-rating' tutor='$tutor->id' class='delete-tutor-rating btn btn-info'> Delete Your Rating </button>";
							echo "</div>";

							echo "</div>";
						}
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="tutor-section" class="row col-md-8 col-md-offset-2">
	<ul id="tutor-navigation" class="nav nav-tabs mid-blue-link">
		<li> 
			<a id='tutor-current-session-tab' href="#tutor_current_session" data-toggle="tab">
				<?
					if($currentTutorSession) {
						echo "Current Session";
					}
					else echo "Declare a Session";
				?>
			</a>
		</li>
		<li> 
			<a href="#tutor_past_sessions" data-toggle="tab">Session History
				<span class="badge"><? echo sizeof($pastTutorSessions)?></span>
			</a> 
		</li>
	</ul>

	<div class="container-fluid">
		<div class="row">
			<div id="tutor-tabs-content" class="col-md-12 tab-content">
				<div class="tab-pane" id="tutor_current_session">
					<div id="current-tutor-session">
						<?php
							if($currentTuteeSession) {
								echo "<p>You are currently enrolled in a tutee session, start session disabled</p>";
							}
							else if($tutor != false) {
								if($currentTutorSession) {
									echo $currentTutorSession->toHtmlCurrent();
								}
								else { #only if there is no current tutee session
									echo "<p>Start a session by filling the form below: </p>";
									echo $tutor_start_session_partial;
								}
							}
						?>
					</div>
				</div>
				<div class="tab-pane" id="tutor_past_sessions">
					<div id="past-tutor-sessions">
						<?php
							if($tutor) {
								echo "<h1>Past Tutor Sessions:</h1>";
								if(sizeof($pastTutorSessions) < 1) {
									echo "<p>No past tutor sessions<p>";
								}
								else {
									for($i=0; $i<sizeof($pastTutorSessions); $i++) {
										echo $pastTutorSessions[$i]->toHtmlPast();
									}
								}
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

</body>

</html>