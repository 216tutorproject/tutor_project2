<!DOCTYPE html>

<?
include($_SERVER['DOCUMENT_ROOT']."/tutor_management/assets/bootstrap/source.php");
?>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Sign In</title>
    <script type="text/javascript" src="../assets/scripts/jquery-1.11.1.min.js"></script>        
    <script type="text/javascript" src="../assets/scripts/jquery-ui-1.8.23.custom.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../assets/stylesheets/customStyles.css"></style>
    <? echo $bootstrapSource; ?>
</head>

<body>

<div class="sign-in-background">
<div class="container">
	<div class="narrow center">

	<div class="row col-md-4-offset-4 center-text">
		<?php
			session_start(); 
			if(isset($_SESSION['failed_sigin']) && $_SESSION['failed_sigin'] == true) {
				echo "<div id='error_msg'> <p> The id and password combination you submitted was invalid </p> </div>";
				session_unset();
			}
		?>
	</div>

	<div class = "well sign-in-box">
		<form class="form-sigin" method="post" action='../index.php'>
			<fieldset class="center-text">
				<legend>Tutor Services | Login</legend>
				<input type="hidden" name="grp" value="Sessions"/>
				<input type="hidden" name="cmd" value="sign_in"/>
				<input type="text" class="form-control" name="uni_id" placeholder="University ID"/>
				<input type="password" class="form-control" name="password" placeholder="Password"/>
				<input type="submit" class="btn btn-primary" name="btn" value="Sign In"/>
			</fieldset>
		</form>
	</div>

	</div>


</div>
</div>
</body>

</html>